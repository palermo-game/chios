import {Server} from 'ws';
const wss = new Server({port: 8080});

console.log(`

██████╗  █████╗ ██╗     ███████╗██████╗ ███╗   ███╗ ██████╗ 
██╔══██╗██╔══██╗██║     ██╔════╝██╔══██╗████╗ ████║██╔═══██╗
██████╔╝███████║██║     █████╗  ██████╔╝██╔████╔██║██║   ██║
██╔═══╝ ██╔══██║██║     ██╔══╝  ██╔══██╗██║╚██╔╝██║██║   ██║
██║     ██║  ██║███████╗███████╗██║  ██║██║ ╚═╝ ██║╚██████╔╝
╚═╝     ╚═╝  ╚═╝╚══════╝╚══════╝╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ 
                                                            
Palermo vUNRELEASED. (c) 2020 Charalampos Fanoulis, Edward Pasenidis, George Tsatsis

Starting chios...
`);

wss.on('connection', ws => {
	ws.on('message', message => {
		if (!message) {
			return;
		}

		console.log('received: %s', message);
	});
});
